export enum ApiStates {
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
}

export const PageSize = 10;
export const RowCount = 2000;

export const UrlBase =
    'https://us-central1-veertly-dev-8b81f.cloudfunctions.net/fetchParticipants';
