import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {UserView} from './UserView'
import { User } from '../interfaces';

interface Props {
    open: boolean;
    handleClose: () => void;
    user: User;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        }
    })
);

export const UserModal = (props: Props) => {
    const classes = useStyles();
    const { open, handleClose, user } = props;
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby='simple-modal-title'
            aria-describedby='simple-modal-description'
            className={classes.modal}>
            <UserView user={user!} closeModal={handleClose!} />
        </Modal>
    );
};
