import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { User } from '../interfaces';


const useStyles = makeStyles({
    root: {
        maxWidth: 400,
    },
    title: {
        fontSize: 18,
    },
    pos: {
        marginBottom: 12,
    },
});

interface Props {
    user: User;
    closeModal: () => void
}

export const UserView = (props: Props) => {
    const classes = useStyles();
    const { user, closeModal } = props;
    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography
                    className={classes.title}
                    color='textSecondary'
                    gutterBottom>
                    {user.firstName} {user.lastName}
                </Typography>
                <Typography className={classes.pos} color='textSecondary'>
                    <div>job: {user.jobTitle}</div>
                    <div>comapny: {user.company}</div>
                    <div>email: {user.email}</div>
                </Typography>
            </CardContent>
            <CardActions>
                <Button size='small' onClick={closeModal}>
                    Close Modal
                </Button>
            </CardActions>
        </Card>
    );
}
