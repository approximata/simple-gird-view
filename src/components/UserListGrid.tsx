import { useState } from 'react';
import {
    DataGrid,
    GridCellParams,
    GridColDef,
    GridPageChangeParams,
    GridValueGetterParams,
} from '@material-ui/data-grid';
import InfoIcon from '@material-ui/icons/Info';

import { ApiStates, PageSize, RowCount } from '../const';
import useApi from '../queries/use-api';
import { User } from '../interfaces';
import {UserModal} from './UserModal';

interface UserMainInfo {
    id: string;
    fullName: string
}

export const UserListGrid = () => {
    const [page, setPage] = useState<number>(0);

    const handlePageChange = (params: GridPageChangeParams) => {
        setPage(params.page);
    };

    const { apiState, apiError, response } = useApi(page);
    const [open, setOpen] = useState<boolean>(false);
    const [user, setUser] = useState<User>();

    const handleOpen = (id: string) => {
        setUser(response.find(user => user.id === id));
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const columns: GridColDef[] = [
        {
            field: 'fullName',
            headerName: 'Full name',
            sortable: false,
            filterable: false,
            width: 250,
            valueGetter: (params: GridValueGetterParams): UserMainInfo => ({
                id: `${params.getValue('id')}`,
                fullName: `${params.getValue('firstName') || ''} ${
                    params.getValue('lastName') || ''
                }`,
            }),
            renderCell: (params: GridCellParams) => {
                const { id, fullName } = params.value as UserMainInfo;
                return (
                    <div>
                        {fullName}
                        <InfoIcon
                            style={{
                                fontSize: 15,
                                marginLeft: 10,
                                marginTop: 10,
                            }}
                            onClick={() => handleOpen(id)}></InfoIcon>
                    </div>
                );
            },
        },
        {
            field: 'company',
            headerName: 'Company',
            sortable: true,
            filterable: true,
            width: 250,
        },
    ];

    if(apiState === ApiStates.ERROR) {
        return (
        <div>
            <div>Something went wrong. :( </div>
            <pre>{apiError}</pre>
        </div>)
    }

    return (
        <div style={{ height: 700, width: 520 }}>
            <DataGrid
                rows={response}
                columns={columns}
                pageSize={PageSize}
                rowCount={RowCount}
                paginationMode='server'
                onPageChange={handlePageChange}
                loading={apiState === ApiStates.LOADING}
            />
            <UserModal open={open} handleClose={handleClose} user={user!}
            />
        </div>
    );
};
