
import { UserListGrid } from './components/UserListGrid'
import { Header } from './components/Header';
import './App.css';


const App = () => {

    return (
        <div className='App'>
            <Header />
            <UserListGrid />
        </div>
    );
};

export default App;
