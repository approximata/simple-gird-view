import { useState, useEffect, useRef } from 'react';
import { ApiStates, PageSize, UrlBase } from '../const';
import { User } from '../interfaces';


interface Response {
    apiState: string;
    response: User[];
    apiError?: string;
}


const useApi = (page: number): Response => {

    const [response, setResponse] = useState<Response>({
        apiState: ApiStates.LOADING,
        apiError: '',
        response: [],
    });

    const fetchData = useRef((url: string) => {});

     fetchData.current = async (url) => {
         const setPartialResponse = (partialResponse: Response) =>
             setResponse({ ...response, ...partialResponse });

         setPartialResponse({
             apiState: ApiStates.LOADING,
             response: [],
         });
         await fetch(url)
             .then((res) =>
                 res.json().then((json) =>
                     setPartialResponse({
                         apiState: ApiStates.SUCCESS,
                         response: json,
                     })
                 )
             )
             .catch((e) => {
                 setPartialResponse({
                     apiState: ApiStates.ERROR,
                     response: [],
                     apiError: `Fetch failed: ${e}`,
                 });
             });
     };
   
    useEffect(() => {
        const offset = page * PageSize;
        const url = `${UrlBase}?offset=${offset}&limit=${PageSize}`;
        fetchData.current(url);
    }, [page, fetchData]);

    return response;
};

export default useApi;
